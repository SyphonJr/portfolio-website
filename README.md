# portfolio_web

PortFolio website

Using a combination of [this](https://dribbble.com/shots/9873242-Portfolio-Mobile-Responsive-Scroll-Behaviour) and [this](https://dribbble.com/shots/5013135-iOS-Developer-Portfolio)

![image](image/portfolio.jpg)

## Navigation bar
So I'm not sure what the best way is to create a navigation bar. If I want a fluid animation between selections, then my navigation bar has to be on top of my screen widgets.
But I don't want that. I just want a highlighted button for the chosen page, maybe a little hover effect. 
So for this use case I can just let my screens use a layout screen with the navigation bar.

UPDATE:
Ok, I decided to go without a navigation bar since, I don't really have a lot of confident.

## Responsive & adaptive design
Flutter makes it easy to create an adaptive design. There are a lot of different ways to do it.
Current way:
Using LayoutBuilder for every view and checking what the maximum size is.

```dart
Widget _smallBuild();
Widget _midBuild();

return LayoutBuilder(
      builder: (context, constraints) {
        Widget content = _smallBuild();
        if (constraints.maxWidth > 700) {
          content = _midBuild();
        }

        return content;
      },
    );
```

There are a few widgets that helps in creating an adaptive design.
```dart
// A widget that expands a child of a Row, Column, or Flex so that the child fills the available space.
Expanded(
    child: Foo(),
) 
// ==
Flexible(
  fit: FlexFit.tight,
  child: Foo(),
);
```

```dart
// The widget first tries the largest width permitted by the layout constraints. The height of the widget is determined by applying the given aspect ratio to the width, expressed as a ratio of width to height.
AspectRatio(
    aspectRatio: 3/4,
    child: Foo(),
)
```

```dart
// Scales and positions its child within itself according to fit.
FittedBox(
    fit: BoxFit.fill,
    child: Foo(),
)
```

```dart
// A widget that sizes its child to a fraction of the total available space
FractionallySizedBox(
    heightFactor: 1,
    child: Foo(),
    widthFactor: 1,
)
```

## Navigation
Using the newish Navigator 2.0. Definitely a lot different than the first one. There are a lot of components to it.

```dart
return Navigator(
  pages: _buildPages(),
  onPopPage: (Route route, result) {
    return true;
  },
  transitionDelegate:
      NoAnimationTransitionDelegate(), // No animations when navigating to different pages on the web
);
```

There's a navigator widget, that contains the pages of your routestack. The navigator detect whenever the routestack changes and determines what needs to be displayed.
This widget can be used as a standalone, or be used by the RouterDelegate. The RouterDelegate can be used to build the Navigator widget, depending on state or url link, the delegate
is responsible for building it.

There's a RouteInformationParser which can be used to translate urls, and the translated urls are then used by the delegate to determine what to do with it.

There's a transitionDelegate for to create animations based on what is happening with the routeStack.

## Issues & thoughts

1. ImageCodecException: Failed to load network image.

   Sometimes images just don't load in Flutter web.

    Solution:  
    flutter run -d chrome --web-renderer html for debugging  
    https://stackoverflow.com/questions/66060984/flutter-web-image-loading-in-mobile-view-but-not-in-full-view  
    
2. Can't select images / text when dragging a random point on the website. It doesn't feel like a real website.

3. Annoying hashtag after the base url.

4. Can't access images with Image.network (CORS)

   Workaround : String src = "https://sourceofimage.com/image.png"
                Image.network("https://cors.bridged.cc/"+ src)

5. Scrolling feels choppy on Firefox
