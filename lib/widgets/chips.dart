import 'package:flutter/material.dart';

enum Tags { flutter, swift }

extension TagHelpers on Tags {
  Color getColor() {
    switch (this) {
      case Tags.flutter:
        return Color(0xFFC86D5B);
      case Tags.swift:
        return Color(0xFF0000);
      default:
        return Colors.black;
    }
  }

  String getName() {
    switch (this) {
      case Tags.flutter:
        return 'flutter';
      case Tags.swift:
        return 'swift';
      default:
        return this.toString();
    }
  }

  static Tags fromString(String tag) {
    return Tags.values.firstWhere((Tags element) => element.getName() == tag);
  }
}

extension CapitalizeExtension on String {
  String capFirst() {
    if (this.length == 0) return this;
    return '${this[0].toUpperCase()}${this.substring(1)}';
  }
}

class TagChip extends StatelessWidget {
  final Tags tag;

  const TagChip({Key? key, required this.tag}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: tag.getColor(),
          borderRadius: BorderRadius.all(Radius.circular(15))),
      child: Padding(
        padding: EdgeInsets.fromLTRB(10, 5, 10, 5),
        child: Text(
          tag.getName().capFirst(),
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
    );
  }

  //   @override
  // Widget build(BuildContext context) {
  //   return Chip(
  //     labelPadding: EdgeInsets.all(2.0),
  //     label: Text(
  //       tag.getName().capFirst(),
  //       style: TextStyle(
  //         color: Colors.white,
  //       ),
  //     ),
  //     backgroundColor: tag.getColor(),
  //     elevation: 6.0,
  //     shadowColor: Colors.grey[60],
  //     padding: EdgeInsets.all(8.0),
  //   );
  // }
}
