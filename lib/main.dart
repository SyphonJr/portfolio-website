import 'package:flutter/material.dart';
import 'package:portfolio_web/router/app_route_parser.dart';
import 'package:portfolio_web/router/app_router_delegate.dart';
import 'package:portfolio_web/router/app_routes_config.dart';

void main() async {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  // This widget is the root of your application.

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  RouterDelegate<AppRoutesConfig> _routerDelegate = AppRouterDelegate();
  RouteInformationParser<AppRoutesConfig> _routeInformationParser =
      AppRouteInformationParser();
  @override
  Widget build(BuildContext context) {
    return Center(
      child: MaterialApp.router(
        title: 'Michael Chen - Portfolio',
        theme: ThemeData(),
        routerDelegate: _routerDelegate,
        routeInformationParser: _routeInformationParser,
      ),
    );
  }
}
