import 'package:portfolio_web/widgets/chips.dart';

class Project {
  final int id;
  final String? description;
  //final String? photoURL;
  final DateTime createdOn;
  final String name;
  final String url;
  // final List<Tags> tags;
  final List<String> tagsAsString;

  Project(
      this.id,
      this.name,
      this.description, //this.photoURL,
      this.createdOn,
      this.url,
      // this.tags,
      this.tagsAsString);

  Project.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        description = json['description'],

        ///photoURL = json['avatar_url'],
        name = json['name'],
        createdOn = DateTime.parse(json['created_at']),
        url = json['web_url'],
        // tags = (json['tag_list'] as List<dynamic>)
        //     .cast<String>()
        //     .map((String tag) => TagHelpers.fromString(tag.toLowerCase()))
        //     .toList(),
        tagsAsString =
            (json['tag_list'] as List<dynamic>).cast<String>().toList();
}
