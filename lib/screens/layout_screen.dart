import 'package:flutter/material.dart';
import 'package:portfolio_web/router/app_routes_config.dart';

class LayoutScreen extends StatelessWidget {
  final Widget child;
  final Routes currentRoute;

  const LayoutScreen(
      {Key? key, required this.currentRoute, required this.child})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Color(0xFF1A1A1A),
      child: SizedBox.expand(
        child: Center(
          child: ConstrainedBox(
            constraints: BoxConstraints(maxWidth: 1200),
            child: LayoutBuilder(
              builder: (context, constraints) {
                return Scaffold(
                  backgroundColor: Color(0xFF1A1A1A),
                  body: child,
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}

// class LayoutScreen extends StatelessWidget {
//   final Widget child;
//   final Routes currentRoute;

//   const LayoutScreen({
//     Key? key,
//     required this.currentRoute,
//     required this.child,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       color: Colors.grey[50],
//       child: SizedBox.expand(
//         child: Center(
//           child: ConstrainedBox(
//             constraints: BoxConstraints(maxWidth: 1100),
//             child: LayoutBuilder(
//               builder: (context, constraints) {
//                 return Scaffold(
//                   drawerEnableOpenDragGesture: false,
//                   appBar: constraints.maxWidth < 450 ? _buildAppbar() : null,
//                   body: Column(
//                     children: [
//                       if (constraints.maxWidth >= 450)
//                         _buildNavigationBar(context, constraints),
//                       Expanded(
//                         child: child,
//                       ),
//                     ],
//                   ),
//                   drawer: constraints.maxWidth < 450 ? _buildDrawer() : null,
//                 );
//               },
//             ),
//           ),
//         ),
//       ),
//     );
//   }

//   Widget _buildNavigationBar(BuildContext context, BoxConstraints constraints) {
//     return Container(
//       decoration: BoxDecoration(),
//       height: 100,
//       child: Align(
//         alignment: Alignment.bottomCenter,
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.end,
//           children: [
//             _buildTextButton(
//               'Home',
//               Routes.home,
//               () {
//                 (Router.of(context).routerDelegate as AppRouterDelegate)
//                     .configuration = AppRoutesConfig.home();
//               },
//               constraints.maxWidth < 750 ? 18 : 20,
//             ),
//             SizedBox(
//               width: 50,
//             ),
//             _buildTextButton(
//               'Portfolio',
//               Routes.projects,
//               () {
//                 (Router.of(context).routerDelegate as AppRouterDelegate)
//                     .configuration = AppRoutesConfig.projects();
//               },
//               constraints.maxWidth < 750 ? 18 : 20,
//             )
//           ],
//         ),
//       ),
//     );
//   }

//   PreferredSizeWidget _buildAppbar() {
//     return AppBar(
//       leading: Builder(
//         builder: (context) => GestureDetector(
//             onTap: () {
//               Scaffold.of(context).openDrawer();
//             },
//             child: Icon(Icons.menu, color: Colors.black)),
//       ),
//       backgroundColor: Colors.transparent,
//       elevation: 0,
//     );
//   }

//   Drawer _buildDrawer() {
//     return Drawer(
//       child: Builder(
//         builder: (context) => ListView(
//           // Important: Remove any padding from the ListView.
//           padding: EdgeInsets.zero,
//           children: <Widget>[
//             DrawerHeader(
//               child: Container(),
//             ),
//             ListTile(
//               title: Text('Home',
//                   style: TextStyle(
//                       color: currentRoute == Routes.home
//                           ? Color(0xFF04C2C9)
//                           : Colors.black)),
//               onTap: () {
//                 (Router.of(context).routerDelegate as AppRouterDelegate)
//                     .configuration = AppRoutesConfig.home();
//               },
//             ),
//             ListTile(
//               title: Text('Portfolio',
//                   style: TextStyle(
//                       color: currentRoute == Routes.projects
//                           ? Color(0xFF04C2C9)
//                           : Colors.black)),
//               onTap: () {
//                 (Router.of(context).routerDelegate as AppRouterDelegate)
//                     .configuration = AppRoutesConfig.projects();
//               },
//             ),
//           ],
//         ),
//       ),
//     );
//   }

//   Widget _buildTextButton(
//       String title, Routes thisRoute, VoidCallback onPressed, double fontSize) {
//     return TextButton(
//       style: ButtonStyle(
//         foregroundColor: MaterialStateProperty.resolveWith<Color>(
//           (Set<MaterialState> states) {
//             if (states.contains(MaterialState.hovered)) {
//               return Color(0xFF04C2C9);
//             }

//             return currentRoute == thisRoute ? Color(0xFF04C2C9) : Colors.black;
//           },
//         ),
//         overlayColor: MaterialStateProperty.all(Colors.transparent),
//       ),
//       onPressed: onPressed,
//       child: Text(
//         title,
//         style: TextStyle(
//           fontSize: fontSize,
//           fontWeight: FontWeight.w700,
//         ),
//       ),
//     );
//   }
// }
