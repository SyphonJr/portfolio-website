import 'package:flutter/material.dart';
import 'package:portfolio_web/constants.dart';
import 'package:portfolio_web/models/project.dart';
import 'package:portfolio_web/screens/home/introduction_view/introduction_view.dart';
import 'package:portfolio_web/screens/home/projects_view/projects_view_provider.dart';
import 'package:portfolio_web/services/git_projects/gitlab_projects.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class ProjectsView extends StatefulWidget {
  final ScreenType screenType;

  const ProjectsView({Key? key, required this.screenType}) : super(key: key);

  @override
  _ProjectsViewState createState() => _ProjectsViewState();
}

enum StillScrollable { upwards, downwards, both }

class _ProjectsViewState extends State<ProjectsView>
    with TickerProviderStateMixin {
  ScrollController _controller = ScrollController();
  StillScrollable stillScrollable = StillScrollable.downwards;

  late final AnimationController _animationController = AnimationController(
    vsync: this,
    duration: Duration(milliseconds: 3000),
  );

  // FadeTransition that starts delayed, animation is choppy otherwise
  late final Animation<double> _fadeAnimation = CurvedAnimation(
    parent: _animationController,
    curve: Interval(0.5, 0.75, curve: Curves.easeIn),
  );

  // SlideAnimation that starts delayed, animation is choppy otherwise
  late final Animation<Offset> _slideAnimation =
      Tween<Offset>(begin: Offset(0, 0.3), end: Offset.zero).animate(
    CurvedAnimation(
      parent: _animationController,
      curve: Interval(0.4, 0.75, curve: Curves.easeInOut),
    ),
  );

  @override
  void initState() {
    // Setting up animation
    _controller.addListener(_scrollListener);
    _animationController.addListener(() {
      setState(() {});
    });

    _animationController.forward();

    super.initState();
  }

  // Function for the ScrollController listener
  // Listen to position changes in the listview
  void _scrollListener() {
    if (widget.screenType == ScreenType.large) {
      if (_controller.position.atEdge) {
        if (_controller.position.pixels == 0) {
          setState(() {
            stillScrollable = StillScrollable.downwards;
          });
        } else {
          setState(() {
            stillScrollable = StillScrollable.upwards;
          });
        }
      }
      // I don't want to call setState everytime when I scroll
      else {
        if (stillScrollable != StillScrollable.both) {
          setState(() {
            stillScrollable = StillScrollable.both;
          });
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        if (widget.screenType == ScreenType.small) return _buildSmallScreen();
        if (widget.screenType == ScreenType.medium) return _buildMediumScreen();
        return _buildLargeScreen();
      },
    );
  }

  Widget _buildMediumScreen() {
    return ChangeNotifierProvider<ProjectsViewProvider>(
      create: (_) => ProjectsViewProvider(GitLabRepoService()),
      builder: (context, child) {
        if (context.watch<ProjectsViewProvider>().state == ViewState.init)
          return Container();
        if (context.watch<ProjectsViewProvider>().state == ViewState.error)
          return Text('Error fetching projects from Gitlab',
              style: TextStyle(color: Colors.white));

        return SizedBox.expand(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
            child: Center(
              child: SizedBox(
                child: SlideTransition(
                  position: _slideAnimation,
                  child: FadeTransition(
                    opacity: _fadeAnimation,
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Projects',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 26,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        Expanded(
                          child: RawScrollbar(
                            controller: _controller,
                            child: ListView.builder(
                              controller: _controller,
                              itemCount: context
                                  .watch<ProjectsViewProvider>()
                                  .listProjects
                                  .length,
                              itemBuilder: (BuildContext context, int index) {
                                Widget content = _buildProjectItem(
                                  context
                                      .read<ProjectsViewProvider>()
                                      .listProjects[index],
                                );

                                if (index !=
                                    context
                                            .read<ProjectsViewProvider>()
                                            .listProjects
                                            .length -
                                        1) {
                                  content = Column(
                                    children: [content, SizedBox(height: 10)],
                                  );
                                }
                                return content;
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildSmallScreen() {
    return ChangeNotifierProvider<ProjectsViewProvider>(
      create: (_) => ProjectsViewProvider(GitLabRepoService()),
      builder: (context, child) {
        if (context.watch<ProjectsViewProvider>().state == ViewState.init)
          return Container();
        if (context.watch<ProjectsViewProvider>().state == ViewState.error)
          return Text('Error fetching projects from Gitlab',
              style: TextStyle(color: Colors.white));

        return SizedBox.expand(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(15, 0, 15, 20),
            child: Center(
              child: SizedBox(
                child: SlideTransition(
                  position: _slideAnimation,
                  child: FadeTransition(
                    opacity: _fadeAnimation,
                    child: Column(
                      children: [
                        Expanded(
                          child: RawScrollbar(
                            controller: _controller,
                            child: ListView.builder(
                              controller: _controller,
                              itemCount: context
                                  .watch<ProjectsViewProvider>()
                                  .listProjects
                                  .length,
                              itemBuilder: (BuildContext context, int index) {
                                Widget content = _buildProjectItem(
                                  context
                                      .read<ProjectsViewProvider>()
                                      .listProjects[index],
                                );

                                if (index == 0) {
                                  content = Column(
                                    children: [
                                      IntroductionView(
                                          screenType: widget.screenType),
                                      Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          'PROJECTS',
                                          style: TextStyle(
                                            fontSize: 15,
                                            color: Colors.white,
                                            fontWeight: FontWeight.w700,
                                            letterSpacing: 3,
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 10),
                                      content
                                    ],
                                  );
                                }

                                if (index !=
                                    context
                                            .read<ProjectsViewProvider>()
                                            .listProjects
                                            .length -
                                        1) {
                                  content = Column(
                                    children: [
                                      content,
                                      SizedBox(height: 10),
                                    ],
                                  );
                                }
                                return content;
                              },
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget _buildLargeScreen() {
    return ChangeNotifierProvider<ProjectsViewProvider>(
      create: (_) => ProjectsViewProvider(GitLabRepoService()),
      builder: (context, child) {
        if (context.watch<ProjectsViewProvider>().state == ViewState.init)
          return Container();
        if (context.watch<ProjectsViewProvider>().state == ViewState.error)
          return Padding(
            padding: EdgeInsets.symmetric(horizontal: 30),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Projects',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 26,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                SizedBox(height: 15),
                Text(
                  'Error fetching projects from gitlab. Please click on the link below to go directly to the gitlab page.',
                  style: TextStyle(
                    color: Colors.grey[400],
                    fontSize: widget.screenType == ScreenType.small ? 15 : 17,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(height: 10),
                Align(
                  alignment: Alignment.center,
                  child: TextButton(
                    onPressed: () async {
                      if (await canLaunch('https://gitlab.com/SyphonJr')) {
                        await launch('https://gitlab.com/SyphonJr');
                      } else {
                        throw 'Could not launch https://gitlab.com/SyphonJr';
                      }
                    },
                    child: Text(
                      'Link',
                      style: TextStyle(
                        fontSize:
                            widget.screenType == ScreenType.small ? 15 : 17,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                )
              ],
            ),
          );

        return SizedBox.expand(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Center(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  maxHeight: 500,
                ),
                child: SlideTransition(
                  position: _slideAnimation,
                  child: FadeTransition(
                    opacity: _fadeAnimation,
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Projects',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 26,
                              fontWeight: FontWeight.w600,
                            ),
                          ),
                        ),
                        // if (stillScrollable == StillScrollable.upwards ||
                        //     stillScrollable == StillScrollable.both)
                        //   Icon(Icons.arrow_drop_up, color: Colors.white)
                        // else
                        //   Icon(Icons.arrow_drop_up, color: Colors.transparent),
                        SizedBox(height: 15),
                        Expanded(
                          child: RawScrollbar(
                            controller: _controller,
                            child: ListView.builder(
                              controller: _controller,
                              itemCount: context
                                  .watch<ProjectsViewProvider>()
                                  .listProjects
                                  .length,
                              itemBuilder: (BuildContext context, int index) {
                                Widget content = _buildProjectItem(
                                  context
                                      .read<ProjectsViewProvider>()
                                      .listProjects[index],
                                );

                                if (index !=
                                    context
                                            .read<ProjectsViewProvider>()
                                            .listProjects
                                            .length -
                                        1) {
                                  content = Column(
                                    children: [content, SizedBox(height: 10)],
                                  );
                                }
                                return content;
                              },
                            ),
                          ),
                        ),
                        // if (stillScrollable == StillScrollable.downwards ||
                        //     stillScrollable == StillScrollable.both)
                        //   Icon(Icons.arrow_drop_down, color: Colors.white)
                        // else
                        //   Icon(Icons.arrow_drop_down,
                        //       color: Colors.transparent),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  // I don't like the use of all these ternary, might aswell make a separate file with all the text styles.
  Widget _buildProjectItem(Project project) {
    return TextButton(
      onPressed: () async {
        if (await canLaunch(project.url)) {
          await launch(project.url);
        } else {
          throw 'Could not launch $project.url';
        }
      },
      child: Container(
        padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
        decoration: BoxDecoration(
          color: Color(0xFF222224),
          borderRadius: BorderRadius.circular(5),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: project.tagsAsString
                  .map((e) => Text(
                        e.toUpperCase(),
                        style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w500,
                          letterSpacing: 2,
                          fontSize: 12,
                        ),
                      ))
                  .toList(),
            ),
            SizedBox(height: 12),
            Text(
              '${project.name}',
              style: TextStyle(
                color: Colors.white,
                fontSize: widget.screenType == ScreenType.small ? 22 : 24,
                fontWeight: FontWeight.w600,
              ),
            ),
            SizedBox(height: 12),
            Text(
              '${project.description}',
              style: TextStyle(
                color: Colors.grey[400],
                fontSize: widget.screenType == ScreenType.small ? 15 : 17,
                fontWeight: FontWeight.w400,
              ),
            ),
            SizedBox(
              height: widget.screenType == ScreenType.small ? 20 : 35,
            ),
            Row(
              children: [
                Text('Gitlab',
                    style: TextStyle(
                        color: Colors.grey[400],
                        fontSize:
                            widget.screenType == ScreenType.small ? 12 : 14)),
                SizedBox(
                  width: 5,
                ),
                Icon(
                  Icons.open_in_new,
                  color: Colors.grey[400],
                  size: widget.screenType == ScreenType.small ? 14 : 16,
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
