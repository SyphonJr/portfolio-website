import 'package:flutter/foundation.dart';
import 'package:portfolio_web/models/project.dart';
import 'package:portfolio_web/services/git_projects/igit_repo.dart';

enum ViewState { init, idle, loading, error }

class ProjectsViewProvider extends ChangeNotifier {
  final IGitRepoService repo;
  List<Project> listProjects = [];

  // Loading because we fetch items at the start.
  ViewState state = ViewState.init;

  ProjectsViewProvider(this.repo) {
    getAll();
  }

  Future<void> getAll() async {
    try {
      listProjects = await repo.getAll();
      listProjects.forEach((element) {});
      state = ViewState.idle;
      notifyListeners();
    } catch (error) {
      state = ViewState.error;
      notifyListeners();
    }
  }
}
