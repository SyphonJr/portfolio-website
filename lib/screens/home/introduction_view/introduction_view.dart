import 'package:flutter/material.dart';
import 'package:portfolio_web/constants.dart';

class IntroductionView extends StatefulWidget {
  final ScreenType screenType;

  const IntroductionView({Key? key, required this.screenType})
      : super(key: key);

  @override
  _IntroductionViewState createState() => _IntroductionViewState();
}

class _IntroductionViewState extends State<IntroductionView>
    with SingleTickerProviderStateMixin {
  late final AnimationController _animationController =
      AnimationController(vsync: this, duration: Duration(milliseconds: 1500));

  // Put in a little delay because the animation feels choppy otherwise
  late final Animation<double> _animation = CurvedAnimation(
      curve: Interval(0.5, 1, curve: Curves.easeIn),
      parent: _animationController);
  @override
  void initState() {
    _animationController.addListener(() {
      setState(() {});
    });

    // Only animate this when it's a large screen
    if (widget.screenType == ScreenType.large ||
        widget.screenType == ScreenType.medium)
      _animationController.forward();
    else {
      _animationController.value = 100;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.screenType == ScreenType.small) return _buildSmallScreen();
    if (widget.screenType == ScreenType.medium) return _buildMediumScreen();
    return _buildLargeScreen();
  }

  Widget _buildMediumScreen() {
    TextStyle mainTextStyle = TextStyle(
      color: Colors.white,
      fontSize: 28,
      fontWeight: FontWeight.w700,
    );

    TextStyle subTextStyle = TextStyle(
      color: Colors.grey[400],
      fontSize: 20,
    );

    return Padding(
      padding: const EdgeInsets.fromLTRB(20, 40, 20, 50),
      child: FadeTransition(
        opacity: _animation,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SelectableText.rich(
              TextSpan(children: [
                TextSpan(
                  text: 'Hello, I\'m ',
                  style: mainTextStyle,
                ),
                TextSpan(
                  text: 'Michael Chen',
                  style: mainTextStyle.copyWith(
                    color: Color(0xFF04C2C9),
                  ),
                ),
                TextSpan(
                    text: ', an aspiring developer.', style: mainTextStyle),
              ]),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10,
            ),
            SelectableText(
              'In love with Flutter. Currently interested in frontend mobile development.',
              style: subTextStyle,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSmallScreen() {
    TextStyle mainTextStyle = TextStyle(
      color: Colors.white,
      fontSize: 34,
      fontWeight: FontWeight.w700,
    );

    TextStyle subTextStyle = TextStyle(
      color: Colors.grey[400],
      fontSize: 16,
    );

    return Padding(
      padding: EdgeInsets.symmetric(vertical: 40),
      child: SizedBox(
        width: double.infinity,
        child: FadeTransition(
          opacity: _animation,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SelectableText.rich(
                TextSpan(children: [
                  TextSpan(
                    text: 'Hello, I\'m ',
                    style: mainTextStyle,
                  ),
                  TextSpan(
                    text: 'Michael Chen.',
                    style: mainTextStyle.copyWith(
                      color: Color(0xFF04C2C9),
                    ),
                  ),
                ]),
                textAlign: TextAlign.left,
              ),
              SizedBox(
                height: 10,
              ),
              SelectableText(
                'Aspiring developer. In love with Flutter. Currently interested in learning frontend mobile development.',
                style: subTextStyle,
                textAlign: TextAlign.left,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildLargeScreen() {
    TextStyle mainTextStyle = TextStyle(
      color: Colors.white,
      fontSize: 28,
      fontWeight: FontWeight.w700,
    );

    TextStyle subTextStyle = TextStyle(
      color: Colors.grey[400],
      fontSize: 20,
    );

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15),
      child: FadeTransition(
        opacity: _animation,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SelectableText.rich(
              TextSpan(children: [
                TextSpan(
                  text: 'Hello, I\'m ',
                  style: mainTextStyle,
                ),
                TextSpan(
                  text: 'Michael Chen',
                  style: mainTextStyle.copyWith(
                    color: Color(0xFF04C2C9),
                  ),
                ),
                TextSpan(
                    text: ', an aspiring developer.', style: mainTextStyle),
              ]),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 10,
            ),
            SelectableText(
              'In love with Flutter. Currently interested in frontend mobile development.',
              style: subTextStyle,
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }
}
