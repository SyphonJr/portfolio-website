import 'package:flutter/material.dart';
import 'package:portfolio_web/constants.dart';
import 'package:portfolio_web/router/app_routes_config.dart';
import 'package:portfolio_web/screens/home/introduction_view/introduction_view.dart';
import 'package:portfolio_web/screens/layout_screen.dart';
import 'projects_view/projects_view.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return LayoutScreen(
      currentRoute: Routes.home,
      child: LayoutBuilder(
        builder: (context, constraints) {
          // Depending on the size of the screen, show different layouts
          if (constraints.maxWidth < 400) return _buildSmallScreen();
          if (constraints.maxWidth < 720) return _buildMediumScreen();
          return _buildLargeScreen();
        },
      ),
    );
  }

  Widget _buildMediumScreen() {
    return Column(
      children: [
        IntroductionView(screenType: ScreenType.medium),
        Expanded(child: ProjectsView(screenType: ScreenType.medium)),
      ],
    );
  }

  Widget _buildSmallScreen() {
    return Column(
      children: [
        Expanded(
          child: ProjectsView(
            screenType: ScreenType.small,
          ),
        )
      ],
    );
  }

  Widget _buildLargeScreen() {
    return Row(
      children: [
        Expanded(
          child: IntroductionView(
            screenType: ScreenType.large,
          ),
        ),
        Expanded(
          child: ProjectsView(
            screenType: ScreenType.large,
          ),
        )
      ],
    );
  }
}
