import 'package:flutter/foundation.dart';
import 'package:portfolio_web/models/project.dart';
import 'package:portfolio_web/services/git_projects/igit_repo.dart';

enum ViewState { idle, loading, error }

class ProjectsProvider extends ChangeNotifier {
  final IGitRepoService repo;
  List<Project> listProjects = [];

  // Loading because we fetch items at the start.
  ViewState state = ViewState.loading;

  ProjectsProvider(this.repo) {
    getAll();
  }

  Future<void> getAll() async {
    try {
      listProjects = await repo.getAll();
    } catch (error) {
      state = ViewState.error;
      notifyListeners();
    }

    state = ViewState.idle;
    notifyListeners();
  }
}
