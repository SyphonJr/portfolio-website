import 'package:flutter/material.dart';
import 'package:portfolio_web/models/project.dart';
import 'package:portfolio_web/router/app_routes_config.dart';
import 'package:portfolio_web/screens/layout_screen.dart';
import 'package:portfolio_web/screens/projects/projects_provider.dart';
import 'package:portfolio_web/services/git_projects/gitlab_projects.dart';
import 'package:portfolio_web/widgets/chips.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class ProjectsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return LayoutScreen(
      currentRoute: Routes.projects,
      child: ChangeNotifierProvider<ProjectsProvider>(
        lazy: false,
        create: (_) => ProjectsProvider(GitLabRepoService()),
        builder: (context, child) {
          Widget body = Container();
          final ViewState currentState =
              context.watch<ProjectsProvider>().state;

          if (currentState == ViewState.error) {
            // Show error
          }
          return Scaffold(
            backgroundColor: Colors.transparent,
            appBar: AppBar(
              backgroundColor: Colors.transparent,
              elevation: 0,
            ),
            body: body,
          );
        },
      ),
    );
  }
}
