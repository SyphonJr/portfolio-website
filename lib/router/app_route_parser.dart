import 'package:flutter/material.dart';
import 'package:portfolio_web/router/app_routes_config.dart';

class AppRouteInformationParser
    extends RouteInformationParser<AppRoutesConfig> {
  @override
  Future<AppRoutesConfig> parseRouteInformation(
      RouteInformation routeInformation) async {
    // Not sure whether to return a 404 / error or an empty string (which would be the home screen)
    final uri = Uri.parse(routeInformation.location ?? '');
    // Handle /
    if (uri.pathSegments.length == 0) {
      return AppRoutesConfig.home();
    }

    // Handle /projects
    if (uri.pathSegments.length == 1) {
      if (uri.pathSegments[0] ==
          AppRoutesConfig.projects().uri.pathSegments[0]) {
        return AppRoutesConfig.projects();
      }
    }

    // Handle /projects/{id}
    if (uri.pathSegments.length == 2) {
      var remainder = uri.pathSegments[1];
      var id = int.tryParse(remainder);
      if (id == null) return AppRoutesConfig.unknown();

      if (uri.pathSegments[0] ==
          AppRoutesConfig.projectDetail(id).uri.pathSegments[0]) {
        return AppRoutesConfig.projectDetail(id);
      }
    }

    // Parsed route is unknown to us
    return AppRoutesConfig.unknown();
  }

  @override
  RouteInformation restoreRouteInformation(AppRoutesConfig path) {
    if (path.route == Routes.projects) {
      return RouteInformation(location: path.uri.path);
    }
    if (path.route == Routes.projectDetail) {
      return RouteInformation(location: path.uri.path);
    }
    if (path.route == Routes.home) {
      return RouteInformation(location: path.uri.path);
    }

    // TODO: detailed exception
    throw (Exception());
  }
}
