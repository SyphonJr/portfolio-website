import 'package:flutter/material.dart';
import 'package:portfolio_web/router/app_routes_config.dart';
import 'package:portfolio_web/router/app_transition_delegate.dart';
import 'package:portfolio_web/screens/404/unknown_screen.dart';
import 'package:portfolio_web/screens/home/home_screen.dart';
import 'package:portfolio_web/screens/projects/projects_screen.dart';
import 'package:portfolio_web/screens/projects_detail_screen/projects_detail_screen.dart';

class AppRouterDelegate extends RouterDelegate<AppRoutesConfig>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<AppRoutesConfig> {
  // The current route we're on
  AppRoutesConfig? _configuration;
  AppRoutesConfig? get configuration => _configuration;
  set configuration(AppRoutesConfig? value) {
    if (configuration == value) return;
    _configuration = value;
    notifyListeners();
  }

  @override
  AppRoutesConfig? get currentConfiguration => configuration;

  // This list of pages is currently only usable for the web, since it's not actually a list
  // It only contains one route
  List<Page<dynamic>> _buildPages() {
    List<Page<dynamic>> pages = [];

    // Show homepage on start
    if (configuration == null) {
      pages.add(MaterialPage(
        key: ValueKey(configuration?.key),
        child: HomeScreen(),
      ));
      return pages;
    }

    if (configuration?.route == Routes.home) {
      pages.add(MaterialPage(
        key: ValueKey(configuration?.key),
        child: HomeScreen(),
      ));
    }

    if (configuration?.route == Routes.projects) {
      pages.add(MaterialPage(
        key: ValueKey(configuration?.key),
        child: ProjectsScreen(),
      ));
    }

    if (configuration?.route == Routes.projectDetail) {
      pages.add(MaterialPage(
        key: ValueKey(configuration?.key),
        child: ProjectsDetailScreen(),
      ));
    }

    if (configuration?.route == Routes.unknown) {
      pages.add(MaterialPage(
        key: ValueKey(configuration?.key),
        child: UnknownScreen(),
      ));
    }

    // Default to unknown screens
    if (pages.isEmpty) {
      pages.add(MaterialPage(
        key: ValueKey(configuration?.key),
        child: UnknownScreen(),
      ));
    }

    return pages;
  }

  @override
  Widget build(BuildContext context) {
    return Navigator(
      pages: _buildPages(),
      onPopPage: (Route route, result) {
        return true;
      },
      transitionDelegate:
          NoAnimationTransitionDelegate(), // No animations when navigating to different pages on the web
    );
  }

  @override
  GlobalKey<NavigatorState>? get navigatorKey => GlobalKey<NavigatorState>();

  @override
  Future<void> setNewRoutePath(AppRoutesConfig configuration) async {
    // Add the new route to the list and update our listeners.
    // listRoutes.add(configuration);
    _configuration = configuration;
  }

  void navigateTo(AppRoutesConfig route) {
    _configuration = route;
  }
}
