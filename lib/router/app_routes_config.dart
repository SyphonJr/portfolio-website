enum Routes { home, projects, projectDetail, unknown }

// Config class to create different routes.
class AppRoutesConfig {
  final int? id;
  final Routes route;
  final String key;
  final Uri uri;

  AppRoutesConfig.home()
      : id = null,
        route = Routes.home,
        key = 'homeRoute',
        uri = Uri.parse('/');

  AppRoutesConfig.projects()
      : id = null,
        route = Routes.projects,
        key = 'projectsRoute',
        uri = Uri.parse('/projects');

  AppRoutesConfig.projectDetail(this.id)
      : route = Routes.projectDetail,
        key = 'projectDetailRoute/{id}',
        uri = Uri.parse('projects/{id}');

  AppRoutesConfig.unknown()
      : id = null,
        route = Routes.unknown,
        key = 'unknownRoute',
        uri = Uri.parse(''); // unknown route doesn't have a path
}
