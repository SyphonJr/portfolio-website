import 'package:portfolio_web/models/project.dart';

abstract class IGitRepoService {
  Future<List<Project>> getAll();
  Future<Project> find(id);
}
