import 'dart:convert';

import 'package:portfolio_web/models/project.dart';
import 'package:portfolio_web/services/git_projects/igit_repo.dart';
import 'package:http/http.dart' as http;

class GitLabRepoService extends IGitRepoService {
  final String endpoint =
      'https://gitlab.com/api/v4/users/289768/starred_projects';

  @override
  Future<Project> find(id) async {
    final response = await http.get(
      Uri.parse(endpoint),
    );
    if (response.statusCode == 200) {
      return Project.fromJson(jsonDecode(response.body));
    } else {
      throw Exception();
    }
  }

  @override
  Future<List<Project>> getAll() async {
    final response = await http.get(
      Uri.parse(endpoint),
    );

    if (response.statusCode == 200) {
      Iterable jsonDecoded = jsonDecode(response.body);

      return List<Project>.from(
        jsonDecoded.map(
          (model) {
            return Project.fromJson(model);
          },
        ),
      );
    } else {
      throw Exception('Error retrieving from GitLab');
    }
  }
}
